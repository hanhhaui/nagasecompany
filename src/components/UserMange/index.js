import "./style.css"
import { Row , Col, Accordion,Button,Offcanvas,Spinner,FormCheck,FormGroup,Pagination ,Form} from "react-bootstrap";
import{useState} from 'react'
function Usermange(){
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return(
        <div className='infor4' id="info4">
                         <h2>User management</h2>
                        <div className="kaka">                       
                            <h4>Userlist</h4>
                            <Button className="newuser" style={{backgroundColor:"#4787A2"}} onClick={handleShow}>
                            <i class="fas fa-plus-circle"></i> Create new user
                            </Button>
                        </div>
                        <table className="tb1">
                            
                            <Row className="row1">
                                <Col xs={5}><input className="nvn" placeholder="NVN Code" /></Col>
                                <Col xs={4}><input className="nvn" placeholder="Full name" /></Col>
                                <Col xs={2}><input className="status" placeholder="Stauts" /></Col>
                                <Col xs={1}><Button className="sear"><i class="fas fa-search"></i> Search</Button></Col>
                            </Row>
                            
                            <tr>
                            <Row className="row2">
                                <Col xs={1} className="no">No</Col>
                                <Col xs={2}>NVN Code</Col>
                                <Col xs={2}>Full Name</Col>
                                <Col xs={3}>Email</Col>
                                <Col xs={1}>Department</Col>
                                <Col xs={1}>Status</Col>
                                <Col xs={2}>Activity log</Col>
                            </Row>
                            </tr>
                            <tr>
                            <Row className="row3">
                                <Col xs={1}>1</Col>
                                <Col xs={2}>NVN0001341</Col>
                                <Col xs={2}>Dương Khánh Linh</Col>
                                <Col xs={3}>linhdk@gmail.com</Col>
                                <Col xs={1}>KSB</Col>
                                <Col xs={1}>Active</Col>
                                <Col xs={2}><a href="#">Linhdk/activitylog</a></Col>
                            </Row>
                            </tr>
                            <tr>
                            <Row className="row3"> 
                                <Col xs={1}>2</Col>
                                <Col xs={2}>NVN0001341</Col>
                                <Col xs={2}>Dương Khánh Linh</Col>
                                <Col xs={3}>linhdk@gmail.com</Col>
                                <Col xs={1}>KSB</Col>
                                <Col xs={1}>Active</Col>
                                <Col xs={2}><a href="#">Linhdk/activitylog</a></Col>
                            </Row>
                            </tr>
                            <tr>
                            <Row className="row3">
                                <Col xs={1}>3</Col>
                                <Col xs={2}>NVN0001341</Col>
                                <Col xs={2}>Dương Khánh Linh</Col>
                                <Col xs={3}>linhdk@gmail.com</Col>
                                <Col xs={1}>KSB</Col>
                                <Col xs={1}>Active</Col>
                                <Col xs={2}><a href="#">Linhdk/activitylog</a></Col>
                            </Row>
                            </tr>
                            <tr>
                            <Row className="row3">
                                <Col xs={1}>4</Col>
                                <Col xs={2}>NVN0001341</Col>
                                <Col xs={2}>Dương Khánh Linh</Col>
                                <Col xs={3}>linhdk@gmail.com</Col>
                                <Col xs={1}>KSB</Col>
                                <Col xs={1}>Active</Col>
                                <Col xs={2}><a href="#">Linhdk/activitylog</a></Col>
                            </Row>
                            </tr>
                            <tr>
                                <Row className="page">
                                    <div>
                                        <p className="rowper">Row per page : 25</p>
                                    </div>
                                    <div>
                                        <Pagination>
                                            <Pagination.Prev />
                                            <Pagination.Item>{1}</Pagination.Item>
                                            <Pagination.Next />
                                        </Pagination>
                                    </div>
                                </Row>
                            </tr>
                        </table>
                        <Offcanvas placement='end' show={show} onHide={handleClose}>
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title>Create new user</Offcanvas.Title> 
                                <Spinner animation="border" variant="success" />                         
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <hr/>
                                <p className="pd">NVN code *</p>
                                <input className="ind" type='text' placeholder=""></input>
                                <p className="pd">Full name * *</p>
                                <input className="ind" type='text' placeholder=""></input>
                                <p className="pd">Email *</p>
                                <input className="ind" type='text' placeholder=""></input>
                                <p className="pd">Department *</p>
                                <select className="department" id="depart">
                                    <option value="D9">D9</option>
                                    <option value="D6">D6</option>
                                    <option value="D5">D5</option>
                                    <option value="D4">D4</option>
                                </select>
                                <form className="formdate" action="/action_page.php">
                                    <label className="pd" hrmlfor="birthdaytime">Birthday *</label><br/>
                                    <input className="indi" type="datetime-local" id="birthdaytime" name="birthdaytime"/>
                                </form>

                                <p className="pd">Role *</p>
                                <input className="ind" type='text'/>
                                <div className="check">
                                    <Row className="r1">
                                        <input className="insea" type='text' placeholder="Search"></input>
                                    </Row >
                                    <Row className="r2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="defaultUnchecked" checked/>
                                            <label class="custom-control-label" htmlFor="defaultUnchecked">CS leader</label>
                                        </div>
                                    </Row>
                                    <Row className="r2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="defaultUnchecked"/>
                                            <label class="custom-control-label" htmlFor="defaultUnchecked">CS</label>
                                        </div>
                                    </Row>
                                    <Row className="r2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="defaultUnchecked"/>
                                            <label class="custom-control-label" htmlFor="defaultUnchecked">Sales</label>
                                        </div>
                                    </Row>
                                </div>
                                <Button className="but" variant="light">Cancel</Button> 
                                <Button className="but" variant="success">Create</Button>{' '}
                            </Offcanvas.Body>
                        </Offcanvas>
                </div>
    );
}
export default Usermange