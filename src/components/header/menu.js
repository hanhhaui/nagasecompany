
export const Menu =[
    {
        eventKey:"0",
        icon:"fas fa-th-list",
        name:"Dashboard",
        link:"/Dasboard",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"1",
        icon:"icon fas fa-users",
        name:"Account Management",
        link:"/AccountManagement",
        children:[
            {
                name:"User management",
                link:"/user"
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"2",
        icon:"icon fas fa-calendar-alt",
        name:"PO Management",
        link:"/POManagement",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"3",
        icon:"icon fas fa-shopping-cart",
        name:"Oder Plan Manage",
        link:"/OderPlanManage",
        children:[
            {
                name:"Stock Control",
                link:""
            },
            {
                name:"New Order Plan List",
                link:""
            },
            {
                name:"Pending Confirmation",
                link:""
            },
            {
                name:"Pending Approval",
                link:""
            },
            {
                name:"Genaral Order Plan ",
                link:""
            },
            {
                name:"Cargo Rotation",
                link:""
            }
        ]
    },
    {
        eventKey:"4",
        icon:"icon far fa-file-alt",
        name:"Trading Contract Manage",
        link:"/TradingContractManage",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"5",
        icon:"icon fas fa-envelope-open-text",
        name:"Delivery Order Manage",
        link:"/DeliveryOrderManage",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"6",
        icon:"icon fas fa-door-open",
        name:"Master Data",
        link:"/MasterData",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    
]
    