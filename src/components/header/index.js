import React from "react";
import './header.css';
import {Menu} from './menu';
import { useState } from "react";
import i18n, { t } from 'i18next';

export default function Header(){
    const menu = Menu;
    function changeLanguage(e) {
        i18n.changeLanguage(e.target.value);
    }
    return (
        <div className='top-main'>
                        
                        <div>
                            <ul className="menu-left">
                                <li><i class="fas fa-home"></i></li>  /
                                {/* {
                                    menu.map((item)=>(
                                        <>
                                            <li><a className="aa" href="#"> {item.link}</a></li>
                                        </>
                                    ))
                                } */}
                                <li><a className="aa" href="#"> Account Management</a></li>
                            </ul>
                        </div>
                        <div>
                            <ul className="menu-right">
                                <select onChange={changeLanguage}>
                                    <option value="vi">
                                        {t('Vietnamese')}
                                    </option>
                                    <option value="en">
                                        {t('English')}
                                    </option>
                                </select>
                                <li><i class="fas fa-search"></i></li>|
                                <li><i class="far fa-bell"></i></li>|
                                <li><i class="far fa-user"></i></li>
                            </ul>
                        </div>
                        
                     </div>
    );
};