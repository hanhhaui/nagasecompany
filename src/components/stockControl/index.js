import './stockControl.css';
import { Nav } from 'react-bootstrap';
import { Route, Routes, Link, BrowserRouter as Router, NavLink } from 'react-router-dom';
import Notice from './Notifications';

import Pass from './Password';
import Personal from './PersonalInformation';
function stock() {
    return (
        <div>
            <h4>Profile Setting</h4>

            <Nav className='navlink' variant="tabs"  >
                <Nav.Item>
                    <Link to="PersonalInformation" eventKey="0" >Personal Information</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="Password" eventKey="1">Password</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="Notifications" eventKey="2" >
                        Notification Setting
                    </Link>
                </Nav.Item>
            </Nav>
            <Routes>
                <Route path="PersonalInformation" eventKey="0"  element={< Personal />} />
                <Route path="Password" eventKey="1" element={< Pass />} />
                <Route path="Notifications" eventKey="2" element={< Notice />} />
                </Routes>
            {/* <Routes>
                <Route path="/user" element={<UserMange />} />
                <Route path="/stockControl" element={<StockControl />} />
                <Route path="/notification" element={<Notifi />} />
            </Routes> */}
        </div>
    );
}

export default stock