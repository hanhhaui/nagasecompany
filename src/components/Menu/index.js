import React from "react";
import { Row , Col, Accordion,Button,Offcanvas,Spinner,Form,FormCheck,FormGroup } from "react-bootstrap";
import {Menu} from './menu';
import './menu.css';
import {Link} from 'react-router-dom';
import { Trans, useTranslation } from 'react-i18next';
import { Translation } from 'react-i18next';

export default function FormMenu (){
    const menu = Menu; 

    return(
        
             <div className='menu-bar' id="boxnoidung">
                <div className='logo-small'></div>
                    <div className='menu'>
                    
                        <div className='main-menu'>
                        <Accordion defaultActiveKey="0" flush>
                            {menu.map((item) =>(
                                <>  
                                        <Accordion.Item eventKey={item.eventKey}>
                                        <Accordion.Header id="ac"><i className={item.icon}></i>
                                            <Trans>{item.name}</Trans>
                                        <i class="fas fa-chevron-down icn"></i></Accordion.Header>
                                            <Accordion.Body>
                                               {
                                                   item.children && item.children.map((child) => (
                                                        <>
                                                            <li><Link to={child.link}>{child.name}</Link></li>
                                                        </>
                                                   ))
                                               }
                                            </Accordion.Body>
                                        </Accordion.Item>                               
                                </>
                            ))}
                            </Accordion>
                         </div>
                         
                     </div>
                     <div className='end'></div>
             </div>
    );
};