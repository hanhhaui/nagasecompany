
export const Menu =[
    {
        eventKey:"1",
        icon:"fas fa-th-list",
        name:"Dashboard",
        link:"/Dasboard",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"2",
        icon:"icon fas fa-users",
        name:"Account Management",
        link:"",
        children:[
            {
                name:"User management",
                link:"/user"
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:"/notification"
            }
        ]
    },
    {
        eventKey:"3",
        icon:"icon fas fa-calendar-alt",
        name:"PO Management",
        link:"",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"4",
        icon:"icon fas fa-shopping-cart",
        name:"Oder Plan Manage",
        link:"",
        children:[
            {
                name:"Stock Control",
                link:"/stockControl"
            },
            {
                name:"New Order Plan List",
                link:"/New Order Plan List"
            },
            {
                name:"Pending Confirmation",
                link:"/Pending Confirmation"
            },
            {
                name:"Pending Approval",
                link:"/Pending Approval"
            },
            {
                name:"Genaral Order Plan ",
                link:"/Genaral Order Plan"
            },
            {
                name:"Cargo Rotation",
                link:"/Cargo Rotation"
            }
        ]
    },
    {
        eventKey:"5",
        icon:"icon far fa-file-alt",
        name:"Trading Contract Manage",
        link:"",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"6",
        icon:"icon fas fa-envelope-open-text",
        name:"Delivery Order Manage",
        link:"",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    {
        eventKey:"7",
        icon:"icon fas fa-door-open",
        name:"Master Data",
        link:"",
        children:[
            {
                name:"User management",
                link:""
            },
            {
                name:"Role Management",
                link:""
            },
            {
                name:"Department Manage",
                link:""
            },
            {
                name:"Activity log",
                link:""
            },
            {
                name:"Notification Manage",
                link:""
            }
        ]
    },
    
]
    