import { Col, Row } from "react-bootstrap";
import './Notification.css'

function Notifi(){
    return (
        <div>
            <h4>Notification Management</h4>
            <br/>
            <table>
                <tr>
                    <Row>
                        <Col className="co">Action</Col>
                        <Col>Notification Status</Col>
                    </Row>
                </tr>
                <tr>
                    <Row>
                        <Col  className="co">Account Management</Col>
                        <Col><label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label></Col>
                    </Row>
                </tr>
                <tr>
                    <Row>
                        <Col className="co">Master Data</Col>
                        <Col><label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label></Col>
                    </Row>
                </tr>
                <tr>
                    <Row>
                        <Col className="co">CPO/ FCST Management</Col>
                        <Col><label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label></Col>
                    </Row>
                </tr>
                <tr>
                    <Row>
                        <Col className="co">Order Plan Management</Col>
                        <Col><label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label></Col>
                    </Row>
                </tr>
                <tr>
                    <Row>
                        <Col className="co">PO Management</Col>
                        <Col><label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label></Col>
                    </Row>
                </tr>
                <tr>
                    <Row>
                        <Col className="co">DO Management</Col>
                        <Col><label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label></Col>
                    </Row>
                </tr>
                <tr>
                    <Row>
                        <Col className="co">Stock Turnover Control</Col>
                        <Col><label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label></Col>
                    </Row>
                </tr>
                <tr>
                    <Row>
                        <Col className="co">Report and Dashboard</Col>
                        <Col><label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label></Col>
                    </Row>
                </tr>
                
            </table>
        </div>
    );
}
export default Notifi