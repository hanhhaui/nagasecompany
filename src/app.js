import React from "react";
import { Col, Container, Row ,Modal, Alert} from "react-bootstrap";
import Header from "./components/header";
import FormMenu from "./components/Menu"
import {Routes, Route, Router} from 'react-router-dom'
import UserMange from './components/UserMange'
import StockControl from './components/stockControl'
import Notifi from './components/Notification'
import { Offline, Online } from "react-detect-offline";

import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";

export default function App(props){
    return (
        
        <Container fluid>
            <Offline>
                <Modal>
                    <p>Hanh</p>
                </Modal>
            </Offline>
            <Row>
                <Col className="cool" style={{padding:0}} xs={2}>
                    <FormMenu/>
                </Col>
                <Col xs={10}>
                    <Header/>
                    <Routes>
                        <Route path="/user" element={<UserMange/>} />
                        <Route path="/stockControl" element={<StockControl/>} />
                        <Route path="/notification" element={<Notifi/>}/>
                    </Routes>
                </Col>
            </Row>
        </Container>
    );
}